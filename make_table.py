#!/usr/bin/env python

import argparse
import pickle
import pyspinsph

# Parse the CLI
parser = argparse.ArgumentParser()
parser.add_argument("--phi", dest="phi", default=0, type=float,
        help="Azimuthal angle (default 0)")
parser.add_argument("--mu", dest="mu", default=0, type=float,
        help="Cosine of the latitude angle (default: 0)")
parser.add_argument("--lmax", dest="lmax", default=8, type=int,
        help="Maximum l to calculate (default: 8)")
parser.add_argument("--spin", dest="s", action="append", type=int,
        help="Spin weight(s) to consider (defaults: [-2, 0])")
parser.add_argument("--output", required=True,
        help="Output file name")

args = parser.parse_args()
if args.s is None:
    args.s = [-2, 0]

# Table of coefficients
table = {}
for s in args.s:
  for l in range(args.lmax + 1):
    for m in range(-l, l+1):
      table[(s,l,m)] = pyspinsph.sYlm(s, l, m, args.phi, args.mu)

# Store table in a file
pickle.dump(table, open(args.output, "wb"), 2)
