PySpinSph: Spin Weighted Spherical Harmonics for Python/NumPy
-------------------------------------------------------------

Installing
----------

To compile you will need

- [numpy](http://www.numpy.org)
- [python](https://www.python.org)
- [swig](http://www.swig.org)

After having installed all of the requirements, you can compile and install
PySpinSph

    $ python setup.py install

You can run the unit tests inside PySpinSph with

    import pyspinsph
    pyspinsph.test()


Usage
-----

The main functions provided by PySpinSph are:

* wigner_d: Wigner D function
* wigner_d_dth: Wigner D function, theta derivative
* Ylm: Scalar spherical harmonics
* sYlm: Spin-Weighted spherical harmonics
* sYlm_dph: Spin-Weighted spherical harmonics, phi derivative
* sYlm_dth: Spin-Weighted spherical harmonics, theta derivative


Authors
-------

Sebastiano Bernuzzi

David Radice