#ifndef SPINSPH_VEC_H
#define SPINSPH_VEC_H

#include <complex.h>

#define SPINSPH_SIZERR 1

extern int spinsph_vec_errno;
char * spinsph_vec_strerror(int err);

int wigner_d_vec(
    double * w_out, int nw,
    int l, int m, int s,
    double const * mu_in, int nmu);
int wigner_d_dth_vec(
    double * w_out, int nw,
    int l, int m, int s,
    double const * mu_in, int nmu);
int spharmY_vec(
    double complex * Y_out, int nY,
    int l, int m,
    double const * phi_in, int nphi,
    double const * mu_in, int nmu);
int spinspharmY_vec(
    double complex * Y_out, int nY,
    int s, int l, int m,
    double const * phi_in, int nphi,
    double const * mu_in, int nmu);
int spinspharmY_dth_vec(
    double complex * Y_out, int nY,
    int s, int l, int m,
    double const * phi_in, int nphi,
    double const * mu_in, int nmu);
int spinspharmY_dph_vec(
    double complex * Y_out, int nY,
    int s, int l, int m,
    double const * phi_in, int nphi,
    double const * mu_in, int nmu);

#endif
