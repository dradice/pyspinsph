%module pyspinsph
%{
#define SWIG_FILE_WITH_INIT
#include "spinsph.h"
#include "spinsph_vec.h"
%}

%include "complex.i"
%include "exception.i"
%include "numpy.i"

%header %{
#include <errno.h>
#include "spinsph.h"
#include "spinsph_vec.h"
%}

%init %{
import_array();
%}

%apply (double * IN_ARRAY1, int DIM1){(double const * phi_in, int nphi)}
%apply (double * IN_ARRAY1, int DIM1){(double const * mu_in, int nmu)}
%apply (double * INPLACE_ARRAY1 , int DIM1){(double * w_out, int nw)}
%apply (double complex * INPLACE_ARRAY1, int DIM1){(double complex * Y_out, int nY)}

%exception {
  errno = 0;
  spinsph_vec_errno = 0;
  $action
  if(spinsph_vec_errno != 0) {
    SWIG_exception(SWIG_ValueError, spinsph_vec_strerror(spinsph_vec_errno));
  }
  if(errno != 0) {
    SWIG_exception(SWIG_ValueError, strerror(errno));
  }
}

%feature("autodoc", "1");

%include "spinsph.h"
%include "spinsph_vec.h"

%pythoncode %{

import numpy as np

def make_vec_1(x):
  """
  Vectorize the argument

  Returns
    x_v    : vector version of x (or x)
    scalar : True if x was a scalar
   """
  if isinstance(x, np.ndarray):
    return x, False
  else:
    return np.array([x], dtype=np.float64), True

def make_vec_2(x, y):
  """
  Vectorize the arguments

  Returns
    x_v    : vector version of x (or x)
    y_v    : vector version of y (or y)
    scalar : True if x and y were scalars
  """
  x_v, x_f = make_vec_1(x)
  y_v, y_f = make_vec_1(y)
  if x_v.shape != y_v.shape:
    if x_v.shape == (1,):
      x_v = np.ones_like(y_v)*x_v[0]
    elif y_v.shape == (1,):
      y_v = np.ones_like(x_v)*y_v[0]
    else:
      raise ValueError("Arrays with incompatible shapes: {} and {}".format(
            x_v.shape, y_v.shape))
  return x_v, y_v, x_f and y_f

def wigner_d(l, m, s, mu):
  """
  Wigner D function

  l, m, s: int
  mu: float or ndarray
  """
  mu_v, scalar = make_vec_1(mu)
  w_v = np.empty_like(mu_v)
  wigner_d_vec(w_v, l, m, s, mu_v)
  if scalar:
    return w_v[0]
  else:
    return w_v

def wigner_d_dth(l, m, s, mu):
  """
  Theta-derivative of the Wigner D function

  l, m, s: int
  mu: float or ndarray
  """
  mu_v, scalar = make_vec_1(mu)
  w_v = np.empty_like(mu_v)
  wigner_d_dth_vec(w, l, m, s, mu_v)
  if scalar:
    return w_v[0]
  else:
    return w_v

def Ylm(l, m, phi, mu):
  """
  Scalar spherical harmonics

  l, m: int
  phi, mu: float or ndarray
  """
  phi_v, mu_v, scalar = make_vec_2(phi, mu)
  Y_v = np.zeros(phi_v.shape, dtype=np.complex128)
  spharmY_vec(Y_v, l, m, phi_v, mu_v)
  if scalar:
    return Y_v[0]
  else:
    return Y_v

def sYlm(s, l, m, phi, mu):
  """
  Spin-weighted spherical harmonics

  s, l, m: int
  phi, mu: float or ndarray
  """
  phi_v, mu_v, scalar = make_vec_2(phi, mu)
  sY_v = np.zeros(phi_v.shape, dtype=np.complex128)
  spinspharmY_vec(sY_v, s, l, m, phi_v, mu_v)
  if scalar:
    return sY_v[0]
  else:
    return sY_v

def sYlm_dph(s, l, m, phi, mu):
  """
  Spin-weighted spherical harmonics, phi derivative

  s, l, m: int
  phi, mu: float or ndarray
  """
  phi_v, mu_v, scalar = make_vec_2(phi, mu)
  sY_v = np.zeros(phi_v.shape, dtype=np.complex128)
  spinspharmY_dph_vec(sY_v, s, l, m, phi_v, mu_v)
  if scalar:
    return sY_v[0]
  else:
    return sY_v

def sYlm_dth(s, l, m, phi, mu):
  """
  Spin-weighted spherical harmonics, theta derivative

  s, l, m: int
  phi, mu: float or ndarray
  """
  phi_v, mu_v, scalar = make_vec_2(phi, mu)
  sY_v = np.zeros(phi_v.shape, dtype=np.complex128)
  spinspharmY_dth_vec(sY_v, s, l, m, phi_v, mu_v)
  if scalar:
    return sY_v[0]
  else:
    return sY_v

def test():
  import unittest
  unittest.TextTestRunner().run(unittest.TestLoader().discover("."))

%}
