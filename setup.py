#!/usr/bin/env python

from distutils.core import Extension, setup

setup(
    name = "spinsph",
    version = "0.1",
    description = "Spin Weighted Spherical Harmonics Python Library",
    author = "Sebastiano Bernuzzi, David Radice",
    author_email = "sebastiano.bernuzzi@unipr.it, dradice@caltech.edu",
    ext_modules = [Extension("_pyspinsph",
      ["pyspinsph.i", "spinsph.c", "spinsph_vec.c"])],
    requires = ["numpy"],
    url = "https://bitbucket.org/dradice/pyspinsph"
)
