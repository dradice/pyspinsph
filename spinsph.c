#include <complex.h>
#include <errno.h>
#include <stdlib.h>
#include <tgmath.h>

#include "spinsph.h"

/* factorial */
static double fact(double n)
{
  double f[] = {1,         1,          2,
		6,         24,         120,
		720,       5040,       40320,
		362880,    3628800,    39916800,
		479001600, 6227020800, 87178291200};

  if(n >= 0. && n <= 14.) {
      return f[(int)n];
  }
  else {
      return lgamma(n + 1.);
  }
}


/* associated Legendre polynomial P_l^m(x)
   (adapted from numerical recipes) */
static double alegpoly(int l, int m, double x)
{
  double temp,pll,pmm,pmmp1,somx2;
  int i,ll;

  if (m < 0 || m > l || fabs(x) > 1.0) {
    errno = EDOM;
    return NAN;
  }

  pmm=1.0;
  if (m > 0) {
    somx2=sqrt((1.0-x)*(1.0+x));
    temp=1.0;
    for (i=1;i<=m;i++) {
      pmm *= -temp*somx2;
      temp += 2.0;
    }
  }
  if (l == m)
    return pmm;
  else {
    pmmp1=x*(2*m+1)*pmm;
    if (l == (m+1))
      return pmmp1;
    else {
      for (ll=(m+2);ll<=l;ll++) {
	pll=(x*(2*ll-1)*pmmp1-(ll+m-1)*pmm)/(ll-m);
	pmm=pmmp1;
	pmmp1=pll;
      }
      return pll;
    }
  }

}


/* wrapper for negative m
   m = -M < 0 : P^m_l = P^(-M)_l = (l-M)!/(l+m)| P^M_l */
static double legpolylm(int l, int m, double x)
{

  if (m>=0)
    return alegpoly(l,m,x);

  else
    return ( pow(-1,-m)*fact(l-(-m))/fact(l+(-m))*alegpoly(l,-m,x) );

}


/* Wigner d-function */
double wigner_d_scal(int l, int m, int s, double x)
{
  double dWig = 0.;

  double costhetah = sqrt(0.5*(1.0 + x));
  double sinthetah = sqrt(0.5*(1.0 - x));

  int k;
  int ki = MAX( 0  , m-s );
  int kf = MIN( l+m, l-s );

  for( k = ki; k <= kf; k++ )
    dWig +=
      ( pow(-1.,k) * pow(costhetah,2*l+m-s-2*k) * pow(sinthetah,2*k+s-m) )/
      ( fact(k) * fact(l+m-k) * fact(l-s-k) * fact(s-m+k) );

  return (sqrt(fact(l+m) * fact(l-m) * fact(l+s) * fact(l-s)) * dWig);
}


/* Wigner d-function drvt */
double wigner_d_dth_scal(int l, int m, int s, double x)
{
  double dWig_dth = 0.;

  double costhetah = sqrt(0.5*(1.0 + x));
  double sinthetah = sqrt(0.5*(1.0 - x));

  int k;
  int ki = MAX( 0  , m-s );
  int kf = MIN( l+m, l-s );

  double tmpc,tmps,drvtcs;

  for( k = ki; k <= kf; k++ ) {

    tmpc = pow(costhetah, - 2*k + 2*l + m - s);
    tmps = pow(sinthetah, 2*k - m + s);
    drvtcs = 0.5*( (2*k - m + s) * costhetah*tmpc * tmps/sinthetah -
		   (-2*k + 2*l + m - s) * tmpc/costhetah * tmps*sinthetah );

    dWig_dth += ( pow(-1.,k) * drvtcs )/( fact(k) * fact(l+m-k) * fact(l-s-k) * fact(s-m+k) );
  }

  return (sqrt(fact(l+m) * fact(l-m) * fact(l+s) * fact(l-s)) * dWig_dth);
}


/* scalar spherical harmonic */
double complex spharmY_scal(int l, int m, double phi, double x)
{
  if ((l<0) || (m<-l) || (m>l) || fabs(x) > 1.0) {
    errno = EDOM;
    return NAN;
  }

  double Plm = sqrt((2.0*l+1)*fact(l-m)/(4.*M_PI*fact(l+m))) * legpolylm(l,m, x);

  double rY = Plm * cos((double)(m)*phi);
  double iY = Plm * sin((double)(m)*phi);

  return rY + I*iY;
}


/* spin-weighted spherical harmonic */
double complex spinspharmY_scal(int s, int l, int m, double phi, double x)
{
  if ((l<0) || (m<-l) || (m>l) || fabs(x) > 1.0) {
    errno = EDOM;
    return NAN;
  }
  if (l < abs(s)) {
    return 0;
  }

  double c = pow(-1.,s) * sqrt( (2.0*l+1.)/(4.*M_PI) );
  double dWigner = c * wigner_d_scal(l,m,-s,x);

  double rY = cos((double)(m)*phi) * dWigner;
  double iY = sin((double)(m)*phi) * dWigner;

  return rY + I*iY;
}


/* drvts of spin-weighted spherical harmonic */
double complex spinspharmY_dth_scal(int s, int l, int m, double phi, double x)
{
  if ((l<0) || (m<-l) || (m>l) || fabs(x) > 1.0) {
    errno = EDOM;
    return NAN;
  }
  if (l < abs(s)) {
    return 0;
  }

  double c = pow(-1.,s) * sqrt( (2.0*l+1.)/(4.*M_PI) );
  double dWigner_dth = c * wigner_d_dth_scal(l,m,-s,x);

  double rY = cos((double)(m)*phi) * dWigner_dth;
  double iY = sin((double)(m)*phi) * dWigner_dth;

  return rY + I*iY;
}

double complex spinspharmY_dph_scal(int s, int l, int m, double phi, double x)
{
  if ((l<0) || (m<-l) || (m>l) || fabs(x) > 1.0) {
    errno = EDOM;
    return NAN;
  }
  if (l < abs(s)) {
    return 0;
  }

  double c = pow(-1.,s) * sqrt( (2.0*l+1.)/(4.*M_PI) );
  double dWigner = c * wigner_d_scal(l,m,-s,x);

  double rY = - (double)(m)*sin((double)(m)*phi) * dWigner;
  double iY =   (double)(m)*cos((double)(m)*phi) * dWigner;

  return rY + I*iY;
}
