#ifndef SPINSPH_H
#define SPINSPH_H

#include <complex.h>

/* math macros */
#define MAX(a,b)                \
  ({ typeof (a) _a = (a);       \
    typeof (b) _b = (b);        \
    _a > _b ? _a : _b; })
#define MIN(a,b)		\
  ({ typeof (a) _a = (a);       \
    typeof (b) _b = (b);        \
    _a < _b ? _a : _b; })

double wigner_d_scal(int l, int m, int s, double mu);
double wigner_d_dth_scal(int l, int m, int s, double mu);
double complex spharmY_scal(int l, int m, double phi, double mu);
double complex spinspharmY_scal(int s, int l, int m, double phi, double mu);
double complex spinspharmY_dth_scal(int s, int l, int m, double phi, double mu);
double complex spinspharmY_dph_scal(int s, int l, int m, double phi, double mu);

#endif
