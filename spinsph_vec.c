#include "spinsph.h"
#include "spinsph_vec.h"

#include <stdio.h>

int spinsph_vec_errno = 0;
char * spinsph_vec_strerror(int err) {
  switch(err) {
    case 0:
      return "No error";
    case SPINSPH_SIZERR:
      return "Arrays have incompatible sizes";
    default:
      return "Unkown error";
  }
}

int wigner_d_vec(
    double * w_out, int nw,
    int l, int m, int s,
    double const * mu_in, int nmu) {
  if(nw != nmu) {
    spinsph_vec_errno = SPINSPH_SIZERR;
    return 0;
  }
  for(int i = 0; i < nw; ++i) {
    w_out[i] = wigner_d_scal(l, m, s, mu_in[i]);
  }
  return 0;
}

int wigner_d_dth_vec(
    double * w_out, int nw,
    int l, int m, int s,
    double const * mu_in, int nmu) {
  if(nw != nmu) {
    spinsph_vec_errno = SPINSPH_SIZERR;
    return 0;
  }
  for(int i = 0; i < nw; ++i) {
    w_out[i] = wigner_d_dth_scal(l, m, s, mu_in[i]);
  }
  return 0;
}

int spharmY_vec(
    double complex * Y_out, int nY,
    int l, int m,
    double const * phi_in, int nphi,
    double const * mu_in, int nmu) {
  if(nY != nphi || nphi != nmu) {
    spinsph_vec_errno = SPINSPH_SIZERR;
    return 0;
  }
  for(int i = 0; i < nY; ++i) {
    Y_out[i] = spharmY_scal(l, m, phi_in[i], mu_in[i]);
  }
  return 0;
}

int spinspharmY_vec(
    double complex * Y_out, int nY,
    int s, int l, int m,
    double const * phi_in, int nphi,
    double const * mu_in, int nmu) {
  if(nY != nphi || nphi != nmu) {
    spinsph_vec_errno = SPINSPH_SIZERR;
    return 0;
  }
  for(int i = 0; i < nY; ++i) {
    Y_out[i] = spinspharmY_scal(s, l, m, phi_in[i], mu_in[i]);
  }
  return 0;
}

int spinspharmY_dth_vec(
    double complex * Y_out, int nY,
    int s, int l, int m,
    double const * phi_in, int nphi,
    double const * mu_in, int nmu) {
  if(nY != nphi || nphi != nmu) {
    spinsph_vec_errno = SPINSPH_SIZERR;
    return 0;
  }
  for(int i = 0; i < nY; ++i) {
    Y_out[i] = spinspharmY_dth_scal(s, l, m, phi_in[i], mu_in[i]);
  }
  return 0;
}

int spinspharmY_dph_vec(
    double complex * Y_out, int nY,
    int s, int l, int m,
    double const * phi_in, int nphi,
    double const * mu_in, int nmu) {
  if(nY != nphi || nphi != nmu) {
    spinsph_vec_errno = SPINSPH_SIZERR;
    return 0;
  }
  for(int i = 0; i < nY; ++i) {
    Y_out[i] = spinspharmY_dph_scal(s, l, m, phi_in[i], mu_in[i]);
  }
  return 0;
}

