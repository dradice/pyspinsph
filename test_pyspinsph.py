#!/usr/bin/env python

import numpy as np
import pyspinsph

from math import cos, pi, sin, sqrt
from unittest import main, TestCase

class TestSpinSpharm(TestCase):
  epsilon = 1.0e-15
  def test_spin_0_sph_h(self):
    for phi in [0, pi/2., pi]:
      for th in [0, pi/4., pi/2.]:
        for l in range(5):
          for m in range(-l, l+1):
            Y1 = pyspinsph.Ylm(l, m, phi, cos(th))
            Y2 = pyspinsph.sYlm(0, l, m, phi, cos(th))
            self.assertAlmostEqual(Y1, Y2, delta=self.epsilon)
  def test_spin_m2_sph_h(self):
    for phi in [0, pi/2., pi]:
      for th in [0, pi/4., pi/2.]:
        costh = cos(th)
        sinth = sin(th)
        omcosth = 1. - costh
        opcosth = 1. + costh

        m = -2
        erY = sqrt(5./(64.*pi)) * omcosth**2 * cos(m*phi)
        eiY = sqrt(5./(64.*pi)) * omcosth**2 * sin(m*phi)
        Y1 = erY + 1j*eiY
        Y2 = pyspinsph.sYlm(-2, 2, m, phi, costh)
        self.assertAlmostEqual(Y2, Y2, delta=self.epsilon)

        m = -1
        erY = sqrt(5./(16.*pi)) * sinth * omcosth * cos(m*phi)
        eiY = sqrt(5./(16.*pi)) * sinth * omcosth * sin(m*phi)
        Y1 = erY + 1j*eiY
        Y2 = pyspinsph.sYlm(-2, 2, m, phi, costh)
        self.assertAlmostEqual(Y2, Y2, delta=self.epsilon)

        m = 0
        erY = sqrt(15./(32.*pi)) * sinth**2
        eiY = 0.
        Y1 = erY + 1j*eiY
        Y2 = pyspinsph.sYlm(-2, 2, m, phi, costh)
        self.assertAlmostEqual(Y2, Y2, delta=self.epsilon)

        m = 1
        erY = sqrt(5./(16.*pi)) * sinth * omcosth * cos(m*phi)
        eiY = sqrt(5./(16.*pi)) * sinth * omcosth * sin(m*phi)
        Y1 = erY + 1j*eiY
        Y2 = pyspinsph.sYlm(-2, 2, m, phi, costh)
        self.assertAlmostEqual(Y2, Y2, delta=self.epsilon)

        m = 2
        erY = sqrt(5./(64.*pi)) * omcosth**2 * cos(m*phi)
        eiY = sqrt(5./(64.*pi)) * omcosth**2 * sin(m*phi)
        Y1 = erY + 1j*eiY
        Y2 = pyspinsph.sYlm(-2, 2, m, phi, costh)
        self.assertAlmostEqual(Y2, Y2, delta=self.epsilon)

class TestSpinSpharmVec(TestCase):
  def test_spin_0_sph_h(self):
    phi  = np.array([0, pi/2., pi])
    th   = np.array([0, pi/4., pi/2.])
    Ystd = np.zeros(3, dtype=np.complex128)
    for i in range(phi.shape[0]):
      for l in range(5):
        for m in range(-l, l+1):
          Ystd[i] = pyspinsph.sYlm(0, l, m, phi[i], cos(th[i]))
    Yvec = pyspinsph.sYlm(0, l, m, phi, np.cos(th))
    for i in range(phi.shape[0]):
      self.assertEqual(Ystd[i], Yvec[i])
  def test_wigner_d(self):
    x   = np.array([0., 0.5, 1.])
    w   = pyspinsph.wigner_d(2, 2, -2, x)
    self.assertIsInstance(w, np.ndarray)
    self.assertEqual(w.shape, x.shape)
    w   = pyspinsph.wigner_d(2, 2, -2, 0.5)
    self.assertIsInstance(w, float)

if __name__ == '__main__':
  main()
